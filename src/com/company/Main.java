package com.company;
class Vehicle {
  int passengers, wheels, maxspeed, burnup;

    //cream o functie nou ce va calcula distanta parcursa, functia va primi un parametru interval ce va seta timpul
    void distance( double interval ){
        double value = maxspeed * interval;
        System.out.println("va parcurge o distanta egala cu "+ value + "km.");
    }
}
public class Main {


    public static void main(String[] args) {

        Vehicle masina = new Vehicle();
        masina.passengers = 2;
        masina.wheels = 4;
        masina.maxspeed = 140;
        masina.burnup = 30;

       // alt vehiclu
       Vehicle autobus = new Vehicle();
       autobus.passengers = 45;
       autobus.wheels = 4;
       autobus.maxspeed = 100;
       autobus.maxspeed = 25;

       //calcularea traseului parcurs in 0.5 ore
        double time = 0.5;
        System.out.println("Automobilul cu "+ masina.passengers + " pasager");
        masina.distance(time);
        System.out.println("Automobilul cu "+ autobus.passengers + " pasager");
        autobus.distance(time);
    }
}
